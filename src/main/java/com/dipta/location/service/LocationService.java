package com.dipta.location.service;

import java.util.List;

import com.dipta.location.entities.Location;

public interface LocationService {

	Location saveLocation(Location location);

	Location updateLocation(Location location);

	void deletelocation(Location location);

	Location getLocationByID(int id);

	List<Location> getAllLocation();
}
