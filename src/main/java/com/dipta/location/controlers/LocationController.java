package com.dipta.location.controlers;

import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.dipta.location.entities.Location;
import com.dipta.location.repoes.LocationRepository;
import com.dipta.location.service.LocationService;
import com.dipta.location.util.EmailUtil;
import com.dipta.location.util.ReportUtil;

@Controller
public class LocationController {
	@Autowired
	EmailUtil emailUtil;
	
	@Autowired
	LocationRepository repository;
	
	@Autowired
	ReportUtil reportutil;
	
	@Autowired
	ServletContext sc;
	
	@Autowired
	private LocationService service;
	public LocationService getService() {
		return service;
	}
	public void setService(LocationService service) {
		this.service = service;
	}
	@RequestMapping("/showCreate")
	public String showCreate(){
		return "createLocation";
	}
	@RequestMapping(value="/saveLoc",method=RequestMethod.POST)
	public String saveLocation(@ModelAttribute("location") Location location, ModelMap modelMap){
		Location LocationSaved = service.saveLocation(location);
		String msg = "Location Saved with id:" +LocationSaved.getId();
		modelMap.addAttribute("msg", msg);
		emailUtil.sendEmail("diptadas73@gmail.com", "Data Inserted", "Data inserted to database Thank you.");
		return "createLocation";
	}
	@RequestMapping(value="displayLocation")
	public String displayLocation(ModelMap modelMap){
		List<Location> location = service.getAllLocation();
		modelMap.addAttribute("location", location);
		return "displayLocation";
	}
	@RequestMapping("deleteLocation")
	public String deleteLocation(@RequestParam("id") int id,ModelMap modelMap){
		Location locdel = new Location();
		locdel.setId(id);
		service.deletelocation(locdel);
		List<Location> location = service.getAllLocation();
		modelMap.addAttribute("location",location);
		return "displayLocation";
	}
	@RequestMapping("/showUpdate")
	public String showUpadate(@RequestParam("id")int id,ModelMap modelMap){
		Location location = service.getLocationByID(id);
		modelMap.addAttribute("location", location);
		return "updateLocation";
		
	}
	@RequestMapping("/updateLoc")
	public String updateLocation(@ModelAttribute("location") Location location, ModelMap modelMap){
		service.updateLocation(location);
		List<Location> locations = service.getAllLocation();
		modelMap.addAttribute("location", locations);
		return "displayLocation";
	}
	@RequestMapping("/generateReport")
	public String generateReport(){
		String path = sc.getRealPath("/");
		List<Object[]> data = repository.findTypeAndTypeCount();
		reportutil.generatePaiChart(path, data);
		return "report";
	}
}
