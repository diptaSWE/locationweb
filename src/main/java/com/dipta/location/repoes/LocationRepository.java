package com.dipta.location.repoes;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.dipta.location.entities.Location;

public interface LocationRepository extends JpaRepository<Location, Integer> {

	@Query(value="SELECT type, COUNT(*) FROM location GROUP BY type", nativeQuery=true)
	public List<Object[]> findTypeAndTypeCount();
}
