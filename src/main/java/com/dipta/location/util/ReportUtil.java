package com.dipta.location.util;

import java.util.List;

public interface ReportUtil {

	void generatePaiChart(String path, List<Object[]> data);
}
